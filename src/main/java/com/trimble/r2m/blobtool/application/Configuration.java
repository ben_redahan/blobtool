package com.trimble.r2m.blobtool.application;

import com.nexala.spectrum.binary.provider.ConfigurationProvider;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class Configuration implements ConfigurationProvider {
	private final Config conf = ConfigFactory.load();

    public Config get() {
        return conf;
    }
    
    public String getQuery(String queryId) {
        return conf.getString("r2m.query." + queryId);
    }

}
