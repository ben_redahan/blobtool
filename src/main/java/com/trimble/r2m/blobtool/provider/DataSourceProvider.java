package com.trimble.r2m.blobtool.provider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

import com.google.inject.Inject;
import com.nexala.spectrum.binary.provider.DataSourceProviderInterface;
import com.trimble.r2m.blobtool.application.Configuration;
import com.typesafe.config.Config;

public class DataSourceProvider implements DataSourceProviderInterface {
    
	private Map<String, DataSource> dataSources;
	
	@Inject
	public DataSourceProvider(Configuration config) {
		dataSources = new HashMap<String, DataSource>();
		
		Config dbConfs = config.get().getConfig("dbConfig");
		List<String> fleetIds = new ArrayList<String>(dbConfs.root().unwrapped().keySet());
        
        for (String fleetId:fleetIds) {
        	Config fleetDbConf = dbConfs.getConfig(fleetId);
        	
        	BasicDataSource bdsx = new BasicDataSource();
            
            bdsx.setInitialSize(10);
            bdsx.setDriverClassName(fleetDbConf.getString("driverClassName"));
            bdsx.setUrl(fleetDbConf.getString("url"));
            bdsx.setUsername(fleetDbConf.getString("user"));
            bdsx.setPassword(fleetDbConf.getString("password"));
            bdsx.setValidationQuery("select 1");
            bdsx.setTimeBetweenEvictionRunsMillis(30000);
            bdsx.setTestWhileIdle(true);
            bdsx.setTestOnBorrow(false);
            bdsx.setTestOnReturn(false);
            
            dataSources.put(fleetId, bdsx);
        }
	}
	
	@Override
    public BasicDataSource getDatasource(String fleetId) {
		return (BasicDataSource) dataSources.get(fleetId);
    }

	@Override
	public Map<String, DataSource> getDatasources() {
        return dataSources;
	}
}
