package com.trimble.r2m.blobtool;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.nexala.spectrum.binary.FileWriterCSV;
import com.nexala.spectrum.binary.bean.ChannelDataBinary;
import com.nexala.spectrum.binary.caching.ChannelBinaryCache;
import com.nexala.spectrum.binary.parse.ChannelValueParser;
import com.nexala.spectrum.binary.provider.ChannelValueBinaryProvider;

/**
 * Blob query tool
 * @author BRedaha
 */
public class App 
{
    public static void main( String[] args )
    {
    	Injector injector = Guice.createInjector(new BlobToolModule());
        
        final ChannelValueBinaryProvider dataProvider;
        final ChannelValueParser channelParser;
        final FileWriterCSV fileWriter = new FileWriterCSV();
        
        
        Options options = new Options();
        options.addOption(Option.builder("f").desc("fleet").required(true).hasArg(true).build());
        options.addOption(Option.builder("s").desc("start time").required(false).hasArg(true).build());
        options.addOption(Option.builder("e").desc("end time").required(false).hasArg(true).build());
        options.addOption(Option.builder("u").desc("unit number").required(false).hasArg(true).build());
        options.addOption(Option.builder("c").desc("column list").required(false).hasArg(true).build());
        
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        
        // parse command line args
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        String fleetId = null;
        if (cmd.hasOption("f")) {
            fleetId = cmd.getOptionValue("f").toUpperCase();
        }
        
        String startTime = LocalDate.now().plusDays(-1L).toString();
        if (cmd.hasOption("s") && !cmd.getOptionValue("s").equals("")) {
            startTime = cmd.getOptionValue("s");
        } else {
            System.out.println("Using default start time: " + startTime);
        }
        
        String endTime = LocalDate.now().toString();
        if (cmd.hasOption("e") && !cmd.getOptionValue("e").equals("")) {
            endTime = cmd.getOptionValue("e");
        } else {
            System.out.println("Using default end time: " + endTime);
        }
        
        String unitNumber = "all";
        if (cmd.hasOption("u") && !cmd.getOptionValue("u").equals("")) {
            unitNumber = cmd.getOptionValue("u");
        }
        
        String colList = "all";
        if (cmd.hasOption("c") && !cmd.getOptionValue("c").equals("")) {
            colList = cmd.getOptionValue("c");
        }
        
        // Testing code
        /*fleetId = "SNG";
        startTime = "2019-01-12";
        endTime = "2019-01-20";
        unitNumber = "2303";
        colList = "ETH_A1_DOOR3_A2_SS_CYCLE_COUNTER";*/
        
                
        // init cache
        ChannelBinaryCache channelCache = injector.getInstance(ChannelBinaryCache.class);
        System.out.println(ChannelBinaryCache.test());
        
        // get data using args
        List<ChannelDataBinary> channelData = new ArrayList<ChannelDataBinary>();
        
        dataProvider = injector.getInstance(ChannelValueBinaryProvider.class);
        if (unitNumber.equals("all")) {
            channelData = dataProvider.getChannelData(fleetId, startTime, endTime, colList);
            System.out.println("Queried " + String.valueOf(channelData.size()) + " channel values.");
        } else {
            channelData = dataProvider.getChannelDataUnit(fleetId, startTime, endTime, colList, unitNumber);
            System.out.println("Queried " + String.valueOf(channelData.size()) + " channel values.");
        }

        // parse data out of the blob (it's is already parsed if we used a subset of columns
        if (colList.equals("all")) {
            channelParser = new ChannelValueParser(channelCache);
            channelData = channelParser.decodeDataSet(fleetId, channelData);
        }
        
        System.out.println("Dataset has: " + String.valueOf(channelData.size()) + " entries");
        
        // create file from data
        try {
            fileWriter.createCSVFile(channelData);
            System.out.println("File write successful.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
}
