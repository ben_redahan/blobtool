@echo on
@REM setlocal
@echo fleet - %1
@echo start - %2
@echo end - %3
@echo unit - %4
@echo channels - %~5

@SET FLEET=%1
@IF NOT "%2" == "" (@SET START_DATE=%2) 	ELSE (@SET START_DATE="")
@IF NOT "%3" == "" (@SET END_DATE=%3) 	ELSE (@SET END_DATE="")
@IF NOT "%4" == "" (@SET UNIT=%4) 				ELSE (@SET UNIT="")
@IF NOT "%~5" == "" (@SET CHANNELS=%~5) 	ELSE (@SET CHANNELS="")

call mvn clean package install

call java -jar target/blobtool-fat.jar -f %FLEET% -s %START_DATE% -e %END_DATE% -u %UNIT% -c %CHANNELS% 